"""Get a dinoipsum uid
TODO: Add redis caching.
"""

import requests
import redis

from django.conf import settings

redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                    port=settings.REDIS_PORT, db=0)

def dinorequest():
    res = requests.get(
        "https://dinoipsum.com/api/?format=json&paragraphs&words=1&paragraphs=1"
    ).json()
    return res

def dinoipsum():
    """Return a dino, either from redis queue if not empty, or from a dinoipsum request"""
    if not redis_instance.exists("dinoqueue") or redis_instance.llen("dinoqueue"):
        dinos = dinorequest()
        for paragraph in dinos:
            for dino in paragraph:
                redis_instance.lpush("dinoqueue", dino)
        return dino
    return redis_instance.rpop("dinoqueue").decode()

def main():
    print(dinoipsum())

if __name__ == "__main__":
    main()