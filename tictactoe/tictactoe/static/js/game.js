Object.defineProperties(Array.prototype, {
    count: {
        value: function(value) {
            return this.filter(x => x==value).length;
        }
    }
});

let currentPlayerElement = document.querySelector('.current-player');

function currentPlayer() {
    return current_state.count('X') >= current_state.count('O') ? 'O': 'X';
}

function matchBoardToState() {
    current_state.forEach((value, index, array) => {
        boardElement.children[index].innerText = playerRepresentation(value);
    })
}

function playerRepresentation(player) {
    switch (player) {
        case 'O': return '◯'
        case 'X': return '⨯'
        default: return ' '
    }
}

function updateCurrentPlayerIndicator() {
    let player = currentPlayer();
    let playerRepr = playerRepresentation(player);
    currentPlayerElement.innerText = playerRepr;
    if (player == "O") {
        boardElement.style.cursor = 'var(--o-player)';
    } else if (player == "X") {
        boardElement.style.cursor = 'var(--x-player)';
    }
}


function applyAction(player, index) {
    if (current_state[index] != ' ') {
        console.error('Server sent an unexpected move');
    } else {
        addMark(player, index);
        updateCurrentPlayerIndicator();
    }
}


function winnerAction(winner) {
    playing = false;
    let message = ""
    if (typeof myself !== 'undefined') {
        if (myself == winner) {
            winnerConfetti();
            message = "You won!";
        } else {
            message = "You lose!";
        }
    } else {
        winnerConfetti();
        let winnerRepr = playerRepresentation(winner);
        message = `${winnerRepr} won!`;
    }
    endMessage(message);
}

function drawAction() {
    endMessage("Draw.");
}


function endMessage(message) {
    let messageBox = document.getElementById('game-message');
    messageBox.innerText = message;
    messageBox.classList.add('animate');
}


function isAlreadyMarked(index) {
    return current_state[index] != ' ';
}

function addMark(player, index) {
    let mark = playerRepresentation(player);
    current_state[index] = player;
    boardElement.children[index].innerText = mark;
    updateCurrentPlayerIndicator(); // FIXME: Might be better to avoid recounting the current player each time 
}


function hasWon(player, state) {
    // Adapted from https://stackoverflow.com/a/58118362/13518918
    const combos = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,4,8],
        [2,4,6],
        [0,3,6],
        [1,4,7],
        [2,5,8],
    ];
    for (let row of combos)  {
        let [a,b,c] = [...row];
        if (state[a] == player && state[b] == player && state[c] == player) {
            return true;
        }
    }
}

function isDraw(state) {
    return state.reduce((prev, curr, index) => {
        return prev && curr != "";
    }, true);
}