
let clickCounter;
let current_state = typeof previous_state !== 'undefined' ? previous_state.split('') : Array.apply(null, Array(9)).map(() => '');
let boardElement = document.querySelector('.board');
let playing = true;

initGame();
updateCurrentPlayerIndicator();

// Add a click event listener to grid cells.
[].slice.call(boardElement.children).forEach((cellDiv, index) => {
    cellDiv.addEventListener("click", markOnClickFunctor(index));
});

function initGame() {
    playing = true;
    // Add a click event listener to grid cells.
    current_state = Array.apply(null, Array(9)).map(() => '');
    matchBoardToState();
    clickCounter = 0;
    updateCurrentPlayerIndicator();
    document.getElementById('game-message').classList.remove('animate');
}

/** Create an event handler for cell at given index */
function markOnClickFunctor(index) {
    return function(event) {
        return markOnClick(index);
    }
}

function markOnClick(index) {
    if (!playing) {
        return;
    }
    // Check no mark is already there
    if (current_state[index ] != '') {
        console.debug('Could not overwrite this cell');
    } else {
        clickCounter++;
        player = clickCounter % 2 == 0 ? 'X': 'O';
        addMark(player, index);
        if (hasWon(player, current_state)) {
            winnerAction(player);
        } else if (isDraw(current_state)) {
            drawAction();
        }
        updateCurrentPlayerIndicator();
    }
}


document.getElementById('online-button').addEventListener('click', (event) => {
    event.preventDefault();
    let roomName = document.getElementsByName('room-id')[0].value;
    if (roomName != "") {
        window.location = "/play/" + roomName;
    } else {
        window.location = "/new";
    }
});

document.getElementById('reset-button').addEventListener('click', (event) => {
    initGame();
});