
const myself = JSON.parse(document.getElementById('player').textContent);
let previous_state = JSON.parse(document.getElementById('previous_state').textContent);
let current_state = typeof previous_state !== 'undefined' ? previous_state.split('') : Array.apply(null, Array(9)).map(() => '');
let playing = !(hasWon('X', current_state) && !(hasWon('O', current_state)));
let boardElement = document.querySelector('.board');


matchBoardToState();
updateCurrentPlayerIndicator();

// Add a click event listener to grid cells.
[].slice.call(boardElement.children).forEach((cellDiv, index) => {
    cellDiv.addEventListener("click", markOnClickFunctor(index));
});

function markOnClickFunctor(index) {
    return function (event) {
        markOnClick(event, index);
    }
}

function markOnClick(event, index) {
    if (playing) {
        // First, check if local state is compatible with a mark click
        if (currentPlayer() != myself)
            console.debug("It's not your turn. Sorry.");
        else if (isAlreadyMarked(index)) {
            console.debug("You cannot play here, the cell is already marked.")
        } else {
            // Ask server permission to mark the cell
            sendMove(index, myself);
        }
    }
}


