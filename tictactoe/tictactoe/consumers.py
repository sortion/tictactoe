import json
import redis

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.conf import settings

from . import tictactoe

redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                    port=settings.REDIS_PORT, db=0)

class GameConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope["url_route"]["kwargs"]["room_name"]
        self.room_group_name = f"room_{self.room_name}"

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name, self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name, self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]

        if 'ask' in message:
            # Check if proposed move is legit
            if not redis_instance.exists(self.room_name):
                response = "Error: no such room in redis cache"
            else:
                current_state = redis_instance.get(self.room_name).decode()
                current_player = 'X' if current_state.count('X') < current_state.count('O') else 'O'
                index = int(message[-1])
                player = message[-2]
                winner = False
                draw = False
                if current_player != player:
                    response = f"wait:{player}"
                elif current_state[index] == ' ':
                    response = f"ok:{player}{index}"
                    # Update current state in redis
                    next_state = list(current_state)
                    next_state[index] = player
                    next_state_repr = "".join(next_state)
                    redis_instance.set(self.room_name, next_state_repr)
                    if tictactoe.is_winner(next_state, player):
                        winner = True
                    elif next_state.count(' ') == 0:
                        draw = True
                else:
                    response = f"ko:{player}{index}"
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.room_group_name, {"type": "game.message", "message": response}
                )
                if winner:
                    async_to_sync(self.channel_layer.group_send)(
                        self.room_group_name, 
                        {"type": "game.message", "message": f"won:{player}"}
                    )
                elif draw:
                    async_to_sync(self.channel_layer.group_send)(
                        self.room_group_name, 
                        {"type": "game.message", "message": f"draw"}
                    )
    

    # Receive message from room group
    def game_message(self, event):
        message = event["message"]

        # Send message to WebSocket
        self.send(text_data=json.dumps({"message": message}))