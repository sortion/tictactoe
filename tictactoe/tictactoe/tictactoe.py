"""Tictactoe logic"""

from typing import Literal

def is_winner(board: list[Literal[' ', 'X', 'O']], player: Literal['X', 'O']):
    """Check is player has three aligned cell

    Parameters:
        state: 
            a 1D list representing the board with either ' ', 'X' or 'O' values.
        player: 
            either 'X' or 'O'
    """
    # Rows
    for row in range(3):
        won: bool = True
        for col in range(3):
            if board[row*3+col] != player:
                won = False
                break
        if won:
            return True
    # Columns
    for col in range(3):
        won = True
        for row in range(3):
            if board[row*3+col] != player:
                won = False
                break
        if won:
            return True
    # Diagonal
    won_diag1 = True
    won_diag2 = True
    for diag in range(3):
        if board[diag*3+diag] != player:
            won_diag1 = False
        if board[((diag*3+diag)+2)%8] != player:
            won_diag2 = False        
    return won_diag1 or won_diag2


def print_board(board: list[Literal[' ', 'X', 'O']]):
    repr = ""
    for row in range(3):
        for col in range(3):
            print(board[row*3+col], end="")
            if col <= 1:
                print(" | ", end="")
        print()
        if row <= 1:
            print("-"*9)

def is_board_full(board: list[Literal[' ', 'X', 'O']]) -> bool:
    for mark in board:
        if mark == ' ':
            return False
    return True


def make_move(board: list[Literal[' ', 'X', 'O']], player: Literal['X', 'O'], row: int, col: int) -> bool:
    index = row*3+col
    if board[index] != ' ':
        return False
    else:
        board[index] = player
        return True


def game_loop():
    mark_counter = 0
    playing = True
    players = ["X", "O"]
    board = [" "]*3**2
    print(board)
    while playing:
        print_board(board)
        player = players[mark_counter%2]
        print(f"{player}'s turn:")
        x = input("x: ")
        if x == "q":
            playing = False
            break
        y = input("y: ")
        if y == "q":
            playing = False
            break
        x = int(x)
        y = int(y)
        if not make_move(board, player, x, y):
            print(f"cannot play at ({x},{y}), please try again")
            continue
        else:
            mark_counter += 1
            if is_winner(board, player):
                print(f"{player} won.")
                end = True
            elif is_board_full(board):
                print("draw.")
                end = True
            else:
                end = False
            if end:
                print("Would you play again?")
                response = input("[Y/n]")
                if response.upper() == "N":
                    playing = False
                else:
                    board = [" "]*3**2


def test():
    state = [
        ' ', ' ', 'X',
        ' ', 'X', ' ',
        'X', ' ', ' '
    ]
    assert is_winner(state, 'X')


def main():
    game_loop()


if __name__ == "__main__":
    main()