"""Tictactoe solver agents (in development)"""

import random

class Brain:
    """ref. Knuth early programs"""

    def possible_actions(self, current_state, player):
        for row in range(3):
            for col in range(3):
                index = row*3+col
                if current_state[index] == " ":
                    yield (player, index)

class StochasticBrain(Brain):
    """Choose an action randomly"""

    def next_action(self, current_state, player):
        return random.choice(self.possible_actions(current_state, player))


class ExpertBrain(Brain):
    """Apply best possible stratetgy"""

    def next_action(self, current_state, player):
        raise NotImplementedError



