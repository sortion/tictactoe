# tictactoe

Implementation of an online multi-player tictactoe game with Django powered websockets.

A small Object Oriented Programming project from M1 GENIOMHE.

<img src=".git-assets/Screenshot_TicTacToe.png" alt="Screenshot of the offline tictactoe game">

## Quickstart

```bash
# Setup venv and install dependencies
cd tictactoe
python3 -m venv .venv/tictactoe
source .venv/tictactoe/bin/activate
pip install -r requirements.txt
# Start development server
python3 manage.py runserver
```

You will need a running redis instance, for instance with:
```bash
docker run  -p 6379:6379 redis:7
```

A docker-compose configuration is available, it may be launched as follows:
```bash
docker compose up -d --build
```

To deploy on the web, you will need a http reverse proxy. A sample nginx configuration is provided in [./conf/nginx.conf](./conf/nginx.conf).

## API

The idea is to use websocket in order to transmit player actions on the board.

The server checks the correctness of all proposed moves from the players, and transmit an Ok message to the requester and forward the move to the other player.

### Tictactoe multiplayer communication protocol

A typical game start by a user creating a new game. A unique party ID is generated and may be shared to another player to join. The first player is randomly selected. 

When both player are ready to play, the board is displayed :TODO:.

The first player clicks on the grid: a message is sent to the server that checks the move validity and transmit the action to players. The server ignores the action from the player if it is not its turn.

The server always check for draw or victory.

The websocket messages are in the following format:

An action is proposed with `ask:Xn` where X means it is an action from the X player, and $n$, that he wants to play at the $n$th cell ($n \in \{ 0, 1, ..., 8 \}$). The other player is represented as an 'O'.
The server may broadcast to all players `ok:Xn` in case the player played a legit move, so that the clients update their tictactoe boards, otherwise it responds `ko:Xn` and the client does not change their board.

If a player `X` send `ask:X` when it's not its turn, the server returns `wait:X`

## Milestones

- [x] Web tictactoe interface
- [x] Allow online multiplayer play with websocket communication
- [x] Add an animation on win or draw
- [ ] Implementation of basic automatic players (ref. Brain{1,2,3} D. E. Knuth)
- [ ] Add other player presence status indicator

## References

* Smeaton, Josh. *Kogame - A real time game in Django*. <https://devblog.kogan.com/blog/kogame-django-channels>
* Django Channels - ReadTheDocs. <https://channels.readthedocs.io/>
